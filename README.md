# i3-sound-switcher

![Picture](media/animated_screenshot.gif)

Switch from headphones and speakers, back and forth, with an i3wm \
keybinding and graphical indicator at i3blocks bar.

USAGE

1-) Put the scripts at the folder of your choice

2-) Edit your i3 config file, something like below \
use the keybinding of you choice,:
* bindsym $mod+t exec [PATH-TO-YOUR-SCRIPTS]/switch-audio-output

3-) Edit your i3blocks.conf file, put the following entry:

* [otp-vol]
* command=[PATH-TO-YOUR-SCRIPTS]/otp-vol
* interval=1

4-) Restart i3-wm environment to see if the changes we're done

Be sure to have installed Font Awesome for correct display at i3blocks bar
